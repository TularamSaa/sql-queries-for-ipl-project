--(Problem-1) Matches per year


SELECT season, COUNT(season)
FROM matches
GROUP BY season;



--(Problem2)
-- Matches own by team per year


SELECT season, winner, COUNT(winner) AS "win per year"
FROM matches
GROUP BY season, winner
ORDER BY season;


--(Problem3)
-- Extra run scored per team in the year 2016
SELECT  deliveries.batting_team ,SUM(deliveries.extra_runs)extra_runs
from deliveries, matches
WHERE matches.season = 2016 AND deliveries.match_id = matches.id
GROUP BY deliveries.batting_team
ORDER BY extra_runs;


--(Problem4)
-- Top 10 economical bowler in the year 2015

SELECT deliveries.bowler,((SUM(deliveries.total_runs)*6)/SUM(CASE WHEN wide_runs=0 THEN 1 ELSE 0 END)) Economy_rate
FROM deliveries, matches
WHERE matches.season = 2015 and deliveries.match_id = matches.id
GROUP BY deliveries.bowler
ORDER BY Economy_rate
LIMIT 10;
